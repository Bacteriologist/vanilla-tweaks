Vanilla Tweaks Plugin:
A time I started learning Java as Goal to understand programming and Minecraft Plugin development. 
As a goal, I chose to make a plugin for Vanilla Survival servers, because I could add a lot of new features that not exists or not work in multiplayer servers. 
Like untaming animals or getting killer bunnies are not possible in vanilla survival. 
Also, Minecraft features as getting the Ender Dragon Egg or sleeping when more players are online is really hard or in most cases impossible. 
As an example, a player only can get the Dragon Egg when he kills the first Ender Dragon or if a player wants to skip the night or rainstorm 
all the players need to sleep.

As inspiration for this the Vanilla Tweaks concept of the British content creator Xisumavoid. 
As of the 1.13 release Xisumavoid is releasing many packs that give others the option to improve their vanilla survival worlds and servers. 
The only problem with this system is that the opportunities of data packs are limited and in many cases inefficient if compared with a Java Plugin. 
That inspired to create a Vanilla Tweaks plugin, for the cases where it is not recommended to use data packs. 
Also, it shows how plugins can improve Vanilla Minecraft while using Minecraft features that already exist.
To make the plugin a success the choices were made to keep the vanilla tweaks simple with the focus on quality. 
The content of the plugin:
The plugin is built out of modules with as min core the Vanilla Tweaks module. This module makes the following tweaks possible.
**Bonus Chest:** Known from single player survival worlds, but now it spawns every player's inventory when they join the server for the first time.
**Multiplayer Sleep:** This feature will make it possible to skip the night when a minimum percentage of the players is sleeping. This percentage is 
adjustable with the config, also it doesn’t interrupt the Minecraft sleeping system if there is only one player online and the night will be skipped with an animation.
**Dragon Egg:** When every player kills the Ender Dragon for the first time the Dragon Egg will spawn on top of the exit portal.  When the player picks up 
this Dragon Egg the plugin will add custom lore to it with the player's name and the date on it. Also, it is very user-friendly because the plugin will always 
find the correct spot for the Dragon Egg and it is also not possible to get more than one egg by cheating.
**Untaming Animals:** With the `/untame` command players can untame their own tamed animals, such as Wolves, Cats, Horses, etc. the only rule is that players 
need to be the owner of that animal. In cases, untamed animals are sitting this will be set automatically to false. If the untamed animal has a chest with items into it, 
then al the items including the chest will be dropped randomly.
**Killer Bunny:** These Rabbits already exists in Minecraft, but they don’t spawn naturally. Now there is a 5% Change that a Killer Bunny will be born while breeding 
Rabbits. This makes rabbits more deadly.



