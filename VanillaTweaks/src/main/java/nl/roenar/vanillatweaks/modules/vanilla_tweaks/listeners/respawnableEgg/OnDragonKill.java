package nl.roenar.vanillatweaks.modules.vanilla_tweaks.listeners.respawnableEgg;


import nl.roenar.vanillatweaks.VanillaTweaksPlugin;
import nl.roenar.vanillatweaks.modules.configuration.ConfigurationManager;
import nl.roenar.vanillatweaks.modules.configuration.objects.User;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;


public class OnDragonKill implements Listener {

    private final VanillaTweaksPlugin plugin;
    private final ConfigurationManager config;

    public OnDragonKill(VanillaTweaksPlugin plugin, ConfigurationManager module) {
        this.plugin = plugin;
        this.config = module;
    }

    /**
     * event that will be called when the Enderdragon is killed by a player.
     * This will only work one time when the player never picked up an dragon egg without lore.
     * @param event the event
     */

    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        LivingEntity entity = event.getEntity();
        if (entity.getKiller() != null) {
            Player player = event.getEntity().getKiller();
            World world = player.getWorld();
            World.Environment environment = world.getEnvironment();

            User user = config.getUser(player);

            if (!user.hasDragonEgg() && environment == World.Environment.THE_END && entity.getType() == EntityType.ENDER_DRAGON) {

                setSpawnEgg(world);

            }
        }
    }

    /**
     * Search for the correct Y value for the Dragon Egg
     * @param world as the world (Only End dimension)
     * @return the Y value
     */

    private int getY(World world) {

        int i = world.getHighestBlockYAt(0, 0);
        int highestY = 255;
        while (i >= 0) {
            if (world.getBlockAt(0, i, 0).getType() == Material.BEDROCK) {
                highestY = i + 1;
                break;
            }
            i--;
        }

        return highestY;
    }

    /**
     * Set the Dragon Egg on the correct spot shortly after the end exit portal is lighted.
     * @param world as the world (Only End dimension)
     */

    private void setSpawnEgg(World world) {
        
        Location loc = new Location(world, 0, getY(world), 0);
        Bukkit.getScheduler().runTaskLater(this.plugin, () -> loc.getBlock().setType(Material.DRAGON_EGG), 250L);
    }

}