package nl.roenar.vanillatweaks.modules.vanilla_tweaks.listeners.respawnableEgg;

import nl.roenar.vanillatweaks.modules.chat.Messages;
import nl.roenar.vanillatweaks.modules.configuration.ConfigurationManager;
import nl.roenar.vanillatweaks.modules.configuration.objects.User;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class PickUpEgg implements Listener {

    private final ConfigurationManager module;

    //private final VanillaTweaksPlugin plugin;

    public PickUpEgg(ConfigurationManager module) {
        this.module = module;
    }

    private static final String LORE_1 = "This egg has been acquired" ;
    private static final String LORE_2 = "by ";
    private static final String LORE_3 = " on " ;

    private final ChatColor AQUA = ChatColor.AQUA;
    private final ChatColor GOLD = ChatColor.GOLD;

    /**
     * Check when a player picked up a dragon egg
     * If the dragon egg is picked up by a player, otherwise it will be cancelled.
     * If the player a custom lore will added with the method addLore.
     * If the player would pickup a dragon egg without lore and user.hasDragonEgg is true the event will be cancelled.
     * @param event as the event
     */

    @EventHandler
    public void onEntityPickupItem(EntityPickupItemEvent event) {
        Entity entity = event.getEntity();
        ItemStack itemStack = event.getItem().getItemStack();

        if(entity instanceof Player && itemStack.getType() == Material.DRAGON_EGG ) {
            Player player = (Player) entity;
            User user = module.getUser(player);
            if(!itemStack.getItemMeta().hasLore() && !user.hasDragonEgg()) {
                addLore(entity, itemStack);
                user.setDragonEgg(true);
                module.saveUser(user);
            } else if(!itemStack.getItemMeta().hasLore() && user.hasDragonEgg()) {
                // To do: avoid that this would be a spambot.
                Messages.message(player, ChatColor.RED + "You already have your own dragon egg");
                event.setCancelled(true);
            }
        } else if (!(entity instanceof Player)) {
            event.setCancelled(true);
        }
    }

    /**
     * Adds custom lore to the dragon egg with the owner of the egg and the date when picked up.
     * @param entity the Entity represent player
     * @param itemStack the Itemstack represent dragon egg
     */

    private void addLore(Entity entity, ItemStack itemStack) {
        Player player = (Player) entity;
        String playerName = player.getName();

        Date date = Calendar.getInstance().getTime();
        DateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        String today = formatter.format(date);

        ItemMeta meta = itemStack.getItemMeta();

        meta.setLore(Arrays.asList(AQUA + LORE_1, AQUA + LORE_2 + GOLD + playerName + AQUA + LORE_3 + GOLD + today + AQUA + "!"));

        itemStack.setItemMeta(meta);
    }
}