package nl.roenar.vanillatweaks.modules.chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Messages {
    /**
     * Messages class
     */

    public static void server_broadcast(String message) {
        Bukkit.getServer().broadcastMessage(ChatColor.DARK_GREEN + "[Server] " + message);
    }

    public static void broadcast(String message) {
        Bukkit.getServer().broadcastMessage(message);
    }

    public static void message(Player player, String message) {
        player.sendMessage(message);
    }

    public static void message(CommandSender sender, String message) {
        sender.sendMessage(message);
    }

    public static void debugger(Player player, String message) {
        player.sendMessage(ChatColor.DARK_RED + "[DEBUG] " + message);
    }

    public static void debugger(String playerMessage) {
        Bukkit.getServer().broadcastMessage(ChatColor.DARK_RED + "[DEBUG] " + playerMessage);
    }




}

