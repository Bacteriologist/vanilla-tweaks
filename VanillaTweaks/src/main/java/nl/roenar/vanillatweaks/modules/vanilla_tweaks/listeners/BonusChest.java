package nl.roenar.vanillatweaks.modules.vanilla_tweaks.listeners;

import nl.roenar.vanillatweaks.VanillaTweaksPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.tags.CustomItemTagContainer;
import org.bukkit.inventory.meta.tags.ItemTagType;
import org.bukkit.loot.LootTables;

import java.util.Arrays;

public class BonusChest implements Listener{


    private final NamespacedKey lootTableKey;

    public BonusChest(VanillaTweaksPlugin plugin) {
        this.lootTableKey = new NamespacedKey(plugin, "LootTable");
    }

    private static final String LORE_1 = "This is the bonus known from ";
    private static final String LORE_2 = "Single Player Survival.";

    /**
     * When the player joined the server for the first time, this will be executed
     * A custom chest is added to the inventory with a Custom tag, necessary to add the loottable
     * in the onPlackPlace event.
     * @param event the join event
     */

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        if(!player.hasPlayedBefore()) {
            ItemStack item = new ItemStack(Material.CHEST, 1);
            ItemMeta meta = item.getItemMeta();

            meta.setDisplayName(ChatColor.DARK_GREEN + "Bonus Chest");
            meta.setLore(Arrays.asList(LORE_1, LORE_2));
            meta.getCustomTagContainer().setCustomTag(lootTableKey, ItemTagType.STRING, "spawn_bonus_chest");

            item.setItemMeta(meta);
            player.getInventory().addItem(item);
        }
    }

    /**
     * Block place event that checks if a player place a chest with a custom tag
     * If itemNBTTag to on the chest "spawn_bonus_chest" it will add the loot table to the chest.
     * It will also remove the custom name of the chest after.
     * @param event the block place event
     */

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (event.getBlockPlaced().getType().equals(Material.CHEST)) {
            ItemStack itemInHand = event.getItemInHand();
            ItemMeta meta = itemInHand.getItemMeta();
            CustomItemTagContainer customItemTagContainer = meta.getCustomTagContainer();
            String itemNBTTag = customItemTagContainer.getCustomTag(lootTableKey, ItemTagType.STRING);

            if("spawn_bonus_chest".equals(itemNBTTag)) {
                Chest chest = (Chest) event.getBlock().getState();

                chest.setCustomName(null);

                chest.setLootTable(Bukkit.getLootTable(LootTables.SPAWN_BONUS_CHEST.getKey()));
                chest.update(true);
            }
        }
    }
}