package nl.roenar.vanillatweaks.modules.vanilla_tweaks.listeners;

import nl.roenar.vanillatweaks.modules.chat.Messages;
import nl.roenar.vanillatweaks.modules.vanilla_tweaks.commands.Untame;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.*;
import org.bukkit.entity.Horse;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.spigotmc.event.entity.EntityMountEvent;

import java.util.UUID;

public class Untaming implements Listener {
    private Untame untame;

    //Getters
    public Untaming(Untame untame) {
        this.untame = untame;
    }

    private boolean hasUUID(UUID uuid) {
        return untame.contains(uuid);
    }

    //Setter
    private void removeUUID(UUID uuid) {
        untame.remove(uuid);
    }

    /**
     * Trigger for untaming Mountable animals, if a player want to
     * untame a mounted animal this event should be cancelled.
     *
     * If a player has used the untame command it will validate if the correct entity is selected.
     * Validation is done in the validation method.
     */

    @EventHandler
    public void onEntityMount(EntityMountEvent event) {
        if (event.getEntityType() == EntityType.PLAYER) {
            Entity entity = event.getMount();
            Player player = (Player) event.getEntity();
            UUID uuid = player.getUniqueId();

            if (hasUUID(uuid)) {
                event.setCancelled(true);
                validation(entity, player, uuid);
            }
        }
    }

    /**
     * Trigger for untaming unMountable animals, if a player want to
     * untame this event should be cancelled.
     *
     * If a player has used the untame command it will validate if the correct entity is selected.
     * Validation is done in the validation method.
     */

    @EventHandler
    public void onPlayerInteractAtEntity(PlayerInteractAtEntityEvent event) {
        Player player = event.getPlayer();
        UUID uuid = player.getUniqueId();

        if(!(event.getRightClicked() instanceof AbstractHorse)) {
            if (hasUUID(uuid)) {

                Entity entity = event.getRightClicked();
                event.setCancelled(true);

                validation(entity, player, uuid);
            }
        }
    }

    /**
     * Validate if the entity is the correct tameable and if the player is the owner is the owner.
     * @param entity the entity
     * @param player the player
     * @param uuid the uuid of the player
     */

    //protocol to check if it is an valid entity.
    private void validation(Entity entity, Player player, UUID uuid) {
        Tameable tameable = (Tameable) entity;

        if (entity instanceof Wolf || entity instanceof Ocelot || entity instanceof Parrot || entity instanceof AbstractHorse) {
            if (tameable.isTamed()) {
                String player_name = player.getName();
                if (tameable.getOwner().getName().equalsIgnoreCase(player_name)) {
                    unTameAnimal(tameable);
                    removeUUID(uuid);

                    Messages.message(player, ChatColor.GREEN + "You selected " + tameable.getName().toLowerCase() + " and this animal is now untamed.");
                } else {
                    Messages.message(player, ChatColor.RED + "You are not the owner of the selected pet.");
                    removeUUID(uuid);
                }
            } else {
                Messages.message(player, ChatColor.RED + "The selected animal isn't tamed.");
                removeUUID(uuid);
            }
        } else {
            Messages.message(player, ChatColor.RED + "The select entity isn't tame able.");
            removeUUID(uuid);
        }
    }

    /**
     * Protocol to untame.
     * If tameable could be sitting after untaming, in that case tameable is sitting it will be set to false.
     * If the tameable is an abstract horse and has a inventory the full inventory will be cleared and the items will be cleared naturally
     * If the abstract horse is carrying a chest this will be set false and the chest will be drop naturally
     * @param tameable as the tameable
     */
    private void unTameAnimal(Tameable tameable) {
        tameable.setTamed(false);

        // Check if the untamed animal can sit to prevent bugs.
        if (tameable instanceof Wolf || tameable instanceof Parrot || tameable instanceof Ocelot) {
            Sittable sittable = (Sittable) tameable;

            // Checks if untamed animal is sitting and if true it will change isSitting to false
            if (sittable.isSitting()) {
                sittable.setSitting(false);
            }
        }

        if (tameable instanceof AbstractHorse) {
            World world = tameable.getWorld();
            Location location = tameable.getLocation();
            AbstractHorse abstractHorse = (AbstractHorse) tameable;
            Inventory inventory = abstractHorse.getInventory();

            for(int i = 0; i < inventory.getSize(); i++) {
                ItemStack itemStack = inventory.getItem(i);
                if (itemStack != null && !itemStack.getType().equals(Material.AIR)) {
                    world.dropItemNaturally(location, itemStack);
                }
            }
            inventory.clear();

            //removes the chests and drops it
            if(!(abstractHorse instanceof Horse)) {
                ChestedHorse chestedHorse = (ChestedHorse) tameable;
                if (chestedHorse.isCarryingChest()) {
                    chestedHorse.setCarryingChest(false);
                    world.dropItemNaturally(location, new ItemStack(Material.CHEST, 1));
                }
            }
        }
    }
}
