package nl.roenar.vanillatweaks.modules.vanilla_tweaks.listeners;

import nl.roenar.vanillatweaks.VanillaTweaksPlugin;
import org.bukkit.entity.Rabbit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityBreedEvent;

import java.util.Random;

public class KillerBunny implements Listener {
    private final VanillaTweaksPlugin plugin;

    private final Rabbit.Type KILLER_BUNNY = Rabbit.Type.THE_KILLER_BUNNY;
    public KillerBunny(VanillaTweaksPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onEntityBreed(EntityBreedEvent event) {

        if (event.getEntity() instanceof Rabbit) {
                Random r = new Random();
                int rInt = r.nextInt(99);

                if(rInt < 5) {
                    Rabbit rabbit = (Rabbit) event.getEntity();

                    rabbit.setRabbitType(KILLER_BUNNY);
                }
            }
        }

}
