package nl.roenar.vanillatweaks.modules.configuration.objects;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SerializableAs("User")
public class User implements ConfigurationSerializable {

    private static final String DATE_FORMAT = "dd MM yyyy";

    private String uuid;
    private String playerName;
    private Date firstJoin;
    private boolean dragonEgg;

    public User(String uuid, String playerName, Date firstJoin, boolean dragonEgg) {
        this.uuid = uuid;
        this.playerName = playerName;
        this.firstJoin = firstJoin;
        this.dragonEgg = dragonEgg;
    }

    public String getUuid() {
        return uuid;
    }

    public String getPlayerName() {
        return playerName;
    }

    public Date getFirstJoin() {
        return firstJoin;
    }

    public boolean hasDragonEgg() {
        return dragonEgg;
    }
    public void setDragonEgg(boolean dragonEgg) {
        this.dragonEgg = dragonEgg;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("uuid", getUuid());
        map.put("name", getPlayerName());
        map.put("firstJoin", new SimpleDateFormat(DATE_FORMAT).format(getFirstJoin()));

        map.put("respawnableDragonEgg", hasDragonEgg());
        return map;
    }

    public static User deserialize(Map<String, Object> map) throws ParseException {
        String uuid = (String) map.get("uuid");
        String name = (String) map.get("name");
        String firstJoin = (String) map.get("firstJoin");
        Date firstJoinDate = new SimpleDateFormat(DATE_FORMAT).parse(firstJoin);
        boolean dragonEgg = Boolean.parseBoolean(map.get("respawnableDragonEgg").toString());

        return new User(uuid, name, firstJoinDate, dragonEgg);
    }

    @Override
    public String toString() {
        return  "User{uuid=" + getUuid() + ",name=" + getPlayerName() + "}";
    }

}
