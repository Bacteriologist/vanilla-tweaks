package nl.roenar.vanillatweaks.modules.configuration;

import nl.roenar.vanillatweaks.VanillaTweaksPlugin;
import nl.roenar.vanillatweaks.modules.configuration.objects.User;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import java.io.File;
import java.io.IOException;

public class ConfigurationManager {
    private final VanillaTweaksPlugin plugin = VanillaTweaksPlugin.getPlugin(VanillaTweaksPlugin.class);

    private static final String USER_METADATA_KEY = "vanillatweaks.user";

    private final File playerDataFolder = new File(plugin.getDataFolder(), "playerdata");

    public void setup() {
        playerDataFolder.mkdirs();
    }

    public User getUser(Player player) {
        return (User) player.getMetadata(USER_METADATA_KEY).get(0).value();
    }

    public void setUser(Player player, User user) {
        player.setMetadata(USER_METADATA_KEY, new FixedMetadataValue(plugin, user));
    }

    public void unsetUser(Player player) {
        player.removeMetadata(USER_METADATA_KEY, plugin);
    }

    public boolean saveUser(Player player) {
        return saveUser(getUser(player));
    }


    /**
     * Saves the user to its config file.
     * @param user the user
     * @return true if saving succeeded, otherwise false
     */
    public boolean saveUser(User user) {
        File playerFile = new File(playerDataFolder, user.getUuid() + ".yml");
        if (!playerFile.exists()) {
            try {
                playerFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(playerFile);
        config.set("user", user);
        try {
            config.save(playerFile);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Loads the user from its config file
     * @param uuid the user's uuid
     * @return a user if retrieved successfully, otherwise null
     */
    public User loadUser(String uuid) {
        File playerFile = new File(playerDataFolder, uuid + ".yml");
        if (!playerFile.exists()) return null;

        FileConfiguration config = YamlConfiguration.loadConfiguration(playerFile);
        User user = (User) config.get("user");

        return user; //can be null
    }
}
