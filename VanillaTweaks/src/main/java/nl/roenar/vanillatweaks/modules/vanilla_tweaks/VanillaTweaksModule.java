package nl.roenar.vanillatweaks.modules.vanilla_tweaks;

import nl.roenar.vanillatweaks.VanillaTweaksPlugin;
import nl.roenar.vanillatweaks.modules.configuration.ConfigurationManager;
import nl.roenar.vanillatweaks.modules.vanilla_tweaks.commands.Untame;
import nl.roenar.vanillatweaks.modules.vanilla_tweaks.listeners.*;
import nl.roenar.vanillatweaks.modules.vanilla_tweaks.listeners.respawnableEgg.OnDragonKill;
import nl.roenar.vanillatweaks.modules.vanilla_tweaks.listeners.respawnableEgg.PickUpEgg;

public class VanillaTweaksModule {

    private VanillaTweaksPlugin plugin;
    private ConfigurationManager configuration;

    public VanillaTweaksModule(VanillaTweaksPlugin plugin, ConfigurationManager configuration) {
        this.plugin = plugin;
        this.configuration = configuration;

        //Commands
        Untame untame = new Untame();
        plugin.getCommand("untame").setExecutor(untame);


        //Listeners
        plugin.getServer().getPluginManager().registerEvents(new BonusChest(plugin), plugin);
        plugin.getServer().getPluginManager().registerEvents(new KillerBunny(plugin), plugin);
        plugin.getServer().getPluginManager().registerEvents(new MultiPlayerSleep(plugin), plugin);
        plugin.getServer().getPluginManager().registerEvents(new Untaming(untame), plugin);
        plugin.getServer().getPluginManager().registerEvents(new OnDragonKill(plugin, configuration), plugin);
        plugin.getServer().getPluginManager().registerEvents(new PickUpEgg(configuration), plugin);
    }
}
