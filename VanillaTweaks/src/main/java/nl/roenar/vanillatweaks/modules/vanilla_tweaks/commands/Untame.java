package nl.roenar.vanillatweaks.modules.vanilla_tweaks.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Untame implements CommandExecutor {
    /**
     * Untaming command, main code is in the class nl.roenar.vanillatweaks.modules.vanilla_tweaks.listeners.Untaming
     */

    private Set<UUID> untame = new HashSet<>();

    public boolean contains(UUID uuid){
        return untame.contains(uuid);
    }

    public void remove(UUID uuid) {
        untame.remove(uuid);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            untame.add(player.getUniqueId());

            sender.sendMessage(ChatColor.GREEN + "Select the entity you want to untame by right clicking on to it.");
            return true;

        } else {
            sender.sendMessage(ChatColor.RED + "Only players can execute this command");
            return true;
        }
    }
}