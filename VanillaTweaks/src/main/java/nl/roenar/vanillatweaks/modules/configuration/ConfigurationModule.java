package nl.roenar.vanillatweaks.modules.configuration;

import nl.roenar.vanillatweaks.VanillaTweaksPlugin;
import nl.roenar.vanillatweaks.modules.configuration.listeners.OnJoinLeave;
import nl.roenar.vanillatweaks.modules.configuration.objects.User;
import org.bukkit.configuration.serialization.ConfigurationSerialization;

public class ConfigurationModule {

    private final VanillaTweaksPlugin plugin;
    private final ConfigurationManager configuration;

    public ConfigurationModule(VanillaTweaksPlugin plugin, ConfigurationManager configuration) {
        this.plugin = plugin;
        this.configuration = configuration;

        ConfigurationSerialization.registerClass(User.class, "User");
        plugin.saveConfig();
        plugin.getConfig().options().copyDefaults(true);

        //Listeners
        plugin.getServer().getPluginManager().registerEvents(new OnJoinLeave(plugin, configuration), plugin);

    }
}
