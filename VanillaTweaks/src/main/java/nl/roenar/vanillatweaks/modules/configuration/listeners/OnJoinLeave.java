package nl.roenar.vanillatweaks.modules.configuration.listeners;

import nl.roenar.vanillatweaks.VanillaTweaksPlugin;
import nl.roenar.vanillatweaks.modules.configuration.ConfigurationManager;
import nl.roenar.vanillatweaks.modules.configuration.objects.User;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Date;

public class OnJoinLeave implements Listener {

    private final VanillaTweaksPlugin plugin;
    private final ConfigurationManager config;


    public OnJoinLeave(VanillaTweaksPlugin plugin, ConfigurationManager config) {
        this.plugin = plugin;
        this.config = config;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerJoin(PlayerLoginEvent event) {
        String userId = event.getPlayer().getUniqueId().toString();
        Player player = event.getPlayer();

        String name = player.getName();

        User user = null;
        if(player.hasPlayedBefore()){
            //player has played before, fetch User object from the config
            user = config.loadUser(userId);
        } else {
            //player has not played before, make new user, save it in config

            user = new User(userId, name, new Date(), false);
            if (!config.saveUser(user)) {
                plugin.getLogger().severe("Couldn't save user: " + user);
            }
        }
        config.setUser(player, user);
    }


    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        config.saveUser(player);
        config.unsetUser(player);
    }

}
