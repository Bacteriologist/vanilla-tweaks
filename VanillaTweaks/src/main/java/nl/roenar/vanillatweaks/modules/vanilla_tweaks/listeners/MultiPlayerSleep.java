package nl.roenar.vanillatweaks.modules.vanilla_tweaks.listeners;

import nl.roenar.vanillatweaks.VanillaTweaksPlugin;
import nl.roenar.vanillatweaks.modules.chat.Messages;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class MultiPlayerSleep implements Listener {

    private final VanillaTweaksPlugin plugin;

    public MultiPlayerSleep(VanillaTweaksPlugin plugin){
        this.plugin = plugin;
    }

    private Set<UUID> playersInBed = new HashSet<>();
    private static final World WORLD= Bukkit.getWorlds().get(0);

    /**
     * Executed when a player went to bed and checks for the amount of players in the overworld.
     * When their are more players in bed then required the night will be skipped smooth.
     * Otherwisa a message will be send.
     * @param event the bed enter event
     */

    @EventHandler
    public void onPlayerBedEnter(PlayerBedEnterEvent event) {
        if(event.getBedEnterResult().toString().equals("OK")) {

            Player player = event.getPlayer();
            String name = player.getName();

            Messages.broadcast(ChatColor.YELLOW + name + " went to bed!");
            playersInBed.add(player.getUniqueId());

            int playersInOverworld = WORLD.getPlayers().size();
            int playersPercentageNeeded = plugin.getConfig().getInt("player_percentage_needed");
            int playersInBedSize = playersInBed.size();
            double playersRequired = (double) playersInOverworld / 100 * playersPercentageNeeded;
            double playersNeeded = Math.round((int) playersRequired - playersInBedSize);

            if ((double) playersInBedSize >= playersRequired && Bukkit.getOnlinePlayers().size() > 1) {
                skipNight();

            } else {
                Messages.server_broadcast(ChatColor.DARK_PURPLE + "People want to sleep, " + ChatColor.DARK_AQUA +
                        playersNeeded + ChatColor.DARK_PURPLE +
                        " more players a required before the night would be skipped.");
            }
        }
    }

    @EventHandler
    public void onPlayerBedLeave(PlayerBedLeaveEvent event){
        Player player = event.getPlayer();
        playersInBed.remove(player.getUniqueId());
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        UUID uuid = event.getPlayer().getUniqueId();

        if(playersInBed.contains(uuid)) {
            playersInBed.remove(uuid);
        }
    }

    /**
     * adds time to the Worlds daytime
     */

    private void addTime() {
        long currentTime = WORLD.getTime();
        long newTime = currentTime + 40;

        WORLD.setTime(newTime);
    }

    /**
     * Protocol to skip the night
     * The runnable is used to speed up the need smooth.
     */
    private int taskId = -1;

    private void skipNight() {
        taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            if (WORLD.getTime() >= 23458 || playersInBed.isEmpty()) {
                Bukkit.getScheduler().cancelTask(taskId);
            } else {
                addTime();
            }
        }, 1L, 1L);
    }

}
