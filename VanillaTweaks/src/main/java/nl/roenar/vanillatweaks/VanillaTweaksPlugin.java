package nl.roenar.vanillatweaks;

import nl.roenar.vanillatweaks.modules.configuration.ConfigurationManager;
import nl.roenar.vanillatweaks.modules.configuration.ConfigurationModule;
import nl.roenar.vanillatweaks.modules.vanilla_tweaks.VanillaTweaksModule;
import org.bukkit.plugin.java.JavaPlugin;

public final class VanillaTweaksPlugin extends JavaPlugin {

    //modules
    private VanillaTweaksModule vanillaTweaksModule;
    private ConfigurationModule configurationModule;
    private ConfigurationManager configurationManager;

    //Getters

    public VanillaTweaksModule getVanillaTweaksModule() {
        return vanillaTweaksModule;
    }

    private ConfigurationManager getConfigurationManager() {
        return configurationManager;
    }

    @Override
    public void onEnable() {

        loadConfigManager();

        this.vanillaTweaksModule = new VanillaTweaksModule(this, configurationManager);
        this.configurationModule = new ConfigurationModule(this, configurationManager);

        loadConfigManager();
        loadConfig();

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic

    }

    private void loadConfigManager() {
        configurationManager = new ConfigurationManager();
        configurationManager.setup();

    }

    private void loadConfig() {
        getConfig().options().copyDefaults(true);
        saveConfig();
    }
}
